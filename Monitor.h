//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Apr 27 09:13:05 2022 by ROOT version 6.22/00
// from TTree PadData/Pad data
// found on file: Output.root
//////////////////////////////////////////////////////////

#ifndef Monitor_h
#define Monitor_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TH1.h>
// Header file for the classes stored in the TTree if any.
// constants
const int NASIC = 20;
const int NCH = 72;
const int NTRIG = 8;
const int NTIME = 20;

class Monitor {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           Event;
   ULong64_t       HEADER0[NASIC];
   ULong64_t       HEADER1[NASIC];
   Int_t           ASIC[NASIC];
   Int_t           ADC[NASIC][NCH];
   Int_t           TOA[NASIC][NCH];
   Int_t           TOT[NASIC][NCH];
   Int_t           TRIGHEADER0[NASIC][NTIME];
   Int_t           TRIGHEADER1[NASIC][NTIME];
   Int_t           TRIGDATA[NASIC][NTRIG][NTIME];

   // List of branches
   TBranch        *b_EventNum;   //!
   TBranch        *b_HEADER0;   //!
   TBranch        *b_HEADER1;   //!
   TBranch        *b_ASICNum;   //!
   TBranch        *b_ADC;   //!
   TBranch        *b_TOA;   //!
   TBranch        *b_TOT;   //!
   TBranch        *b_TRIGHEADER0;   //!
   TBranch        *b_TRIGHEADER1;   //!
   TBranch        *b_TRIGDATA;   //!

   Monitor(TTree *tree=0);
   virtual ~Monitor();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual Int_t    LoadPedestals(const char* filename);
   virtual void     Init(TTree *tree);
   virtual void     Loop(int istart = 0, int istop = 100);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual void 	Init();
   virtual void 	Print(int iasic);
   virtual void 	Reset(int iasic);
   //virtual void 	Plot(int iasic);
   virtual void 	PlotADC(int iasic, const char *name = "");
   virtual void 	PlotTOT(int iasic);
   virtual void 	PlotTOA(int iasic);
   virtual void 	PedestalExtraction();

   virtual void 	ShowADCWoPedestal(Bool_t on, Int_t asic);
   virtual Float_t 	CalculateCommonNoise(Int_t iasic);
   virtual void 	ShowADCWoCommonNoise(Bool_t on, Int_t asic);

   void Run();

   //List of histograms
   TH2D* hADCMap[NASIC];
   TH2D* hADCMapPedRemoved[NASIC];
   TH2D* hADCMapCMNRemoved[NASIC];
   TH1D* hADCDistribution[NASIC][NCH];
   TH1D* hADCDistributionPedRemoved[NASIC][NCH];
   TH1D* hADCDistributionCMNRemoved[NASIC][NCH];
   TH1D* hADCCalib[NASIC][2];
   TH1D* hADCCmn[NASIC][2];
   TH1D* hADCDistributionNew[NASIC][NCH];
   TH1D* hADCCalibNew[NASIC][2];
   TH1D* hADCCmnNew[NASIC][2];
   TH2D* hTriggerData[NASIC][NTRIG];
   TF1* fGaus[NASIC][NCH];

   // Pedestal position & sigma from save file
   Float_t PedValue[NASIC][NCH];
   Float_t PedSigmaValue[NASIC][NCH];

   
   TH2D* hTOAMap[NASIC];
   TH1D* hTOADistribution[NASIC][NCH];
   TH2D* hTOTMap[NASIC];
   TH1D* hTOTDistribution[NASIC][NCH];
};

#endif

#ifdef Monitor_cxx
Monitor::Monitor(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Output.root");
      }
      f->GetObject("PadData",tree);

   }
   Init(tree);
}

Monitor::~Monitor()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Monitor::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Monitor::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

// Load Pedestal data from file 'filename'. returns 1 if succesfully loads
// the file
Int_t Monitor::LoadPedestals(const char* filename)
{
    ifstream fin(filename);
    if (fin.is_open()) {
        int iasic, ich;
        double ped, sigma;
        while (fin>>iasic>>ich>>ped>>sigma) {
            PedValue[iasic][ich]      = ped;
            PedSigmaValue[iasic][ich] = sigma;
        }
        cout << "Pedestals loaded from file '" << filename << "' succesfully" << endl;
        return 1;
    } else {
        cout << "Pedestal file could not be opened! Check that file '"
             << filename
             << "' exists" << endl;
        return 0;
    }
}

void Monitor::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   //fChain->SetBranchAddress("Event", &Event, &b_EventNum);
   fChain->SetBranchAddress("HEADER0", HEADER0, &b_HEADER0);
   fChain->SetBranchAddress("HEADER1", HEADER1, &b_HEADER1);
   fChain->SetBranchAddress("ASIC", ASIC, &b_ASICNum);
   fChain->SetBranchAddress("ADC", ADC, &b_ADC);
   fChain->SetBranchAddress("TOA", TOA, &b_TOA);
   fChain->SetBranchAddress("TOT", TOT, &b_TOT);
   fChain->SetBranchAddress("TRIGHEADER0", TRIGHEADER0, &b_TRIGHEADER0);
   fChain->SetBranchAddress("TRIGHEADER1", TRIGHEADER1, &b_TRIGHEADER1);
   fChain->SetBranchAddress("TRIGDATA", TRIGDATA, &b_TRIGDATA);
   Notify();
}

Bool_t Monitor::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Monitor::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Monitor::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Monitor_cxx
