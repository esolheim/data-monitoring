double mycolor[10] = {kBlack, kRed, kBlue, kMagenta, kGreen+3, kOrange-3, kYellow+3, kCyan-3, kGray+2, kViolet+3};

void mc(int number = 0, int xp = 0, int yp = 0, int x = 400, int y = 400, double pleft = 0.17, double pright = 0.1, double ptop = 0.1, double pbot = 0.13){
  char name[100];
  sprintf(name,"cc%d",number);
  TPad *mpad;
  if(gROOT->GetListOfCanvases()->FindObject(name)==NULL){
  	TCanvas* c = new TCanvas(name,name, xp, yp, x, y);
  	gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
  	gStyle->SetMarkerSize(1.6);
  	c->cd();
    sprintf(name,"mpad%d",number);	
  	mpad = new TPad(name,name,0.02,0.02,0.99,0.99,0,0,0);
  	mpad->SetTopMargin(ptop);
  	mpad->SetBottomMargin(pbot);
  	mpad->SetLeftMargin(pleft);
  	mpad->SetRightMargin(pright);
  	mpad->Range(0, 0, 1, 1);
  	mpad->SetLogz(1);
  	mpad->Draw();
  	mpad->cd();
	c->Modified();
	c->Update();
  }
  else {
  	TCanvas *tmp = (TCanvas*)gROOT->GetListOfCanvases()->FindObject(name);
    tmp->cd();
    mpad->cd();
	tmp->Modified();
	tmp->Update();	
  }
  gSystem->ProcessEvents();
}
void mrat(int number = 0){
    char name[100];
    sprintf(name,"cc%d",number);
    TCanvas *c = new TCanvas(name,name,0,0,800,1000);
    gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
    c->cd();
    TPad *top = new TPad("top","top",0.01,0.01,0.99,0.99,0,0,0);
    top->SetTopMargin(0.0);
    top->SetBottomMargin(0.0);
    top->SetLeftMargin(0.0);
    top->SetRightMargin(0.0);
    top->Range(0, 0, 10, 10);
    top->Draw();
    top->cd();
    //   This is just for the Y axis label
    top->cd();
	sprintf(name,"padu%d",number);	
    TPad *pad01 = new TPad(name,name,0.01,0.35,0.99,0.99,0,0,0);
    pad01->SetTopMargin(0.0);
    pad01->SetBottomMargin(0.0);
    pad01->SetLeftMargin(0.15);
    pad01->SetRightMargin(0.05);
    pad01->Draw();
    pad01->cd();
    top->cd();
	sprintf(name,"padd%d",number);	
    TPad *pad02 = new TPad(name,name,0.01,0.01,0.99,0.35,0,0,0);
    pad02->SetTopMargin(0.0);
    pad02->SetBottomMargin(0.3);
    pad02->SetLeftMargin(0.15);
    pad02->SetRightMargin(0.05);
    pad02->Draw();
    pad02->cd();
}

void mc2x4(int number = 0, double width = 0.235, int x = 1300, int y = 800){
	//============================================================================
	// 							 Four Panel
	//============================================================================
	char name[100];
	double vertical = 0.525;
	sprintf(name,"cc%d",number);
	TCanvas *c = new TCanvas(name,name, 0, 0, x, y);
  	gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
  	gStyle->SetMarkerSize(1.6);
  	c->cd();
  	TPad *top = new TPad("top","top",0.01,0.01,0.99,0.99,0,0,0);
  	top->SetTopMargin(0.0);top->SetBottomMargin(0.0);top->SetLeftMargin(0.0);top->SetRightMargin(0.0);top->Draw();
	top->cd();
	//   This is just for the Y axis label
  	top->cd();
	sprintf(name,"padl%d",number);	
  	TPad *padl3 = new TPad(name,name,0.,vertical,0.25,0.99,0,0,0);padl3->SetTopMargin(0.05);padl3->SetBottomMargin(0.0);padl3->SetLeftMargin(0.214);padl3->SetRightMargin(0.0);padl3->Draw();
	padl3->cd();
	
	//   First panel
  	top->cd();
	sprintf(name,"pad0%d",number);	
  	TPad *pad00 = new TPad(name,name,0.053,vertical,0.053+width,0.99,0,0,0);pad00->SetTopMargin(0.05);pad00->SetBottomMargin(0.00);pad00->SetLeftMargin(0.0);pad00->SetRightMargin(0.0);pad00->Draw();
	pad00->cd();
	//   Second panel
  	top->cd();
	sprintf(name,"pad1%d",number);	
  	TPad *pad10 = new TPad(name,name,0.053+width,vertical,0.053+2*width,0.99,0,0,0);pad10->SetTopMargin(0.05);pad10->SetBottomMargin(0.0);pad10->SetLeftMargin(0.0);pad10->SetRightMargin(0.0);pad10->Draw();
	pad10->cd();
	//   Third panel
  	top->cd();
	sprintf(name,"pad2%d",number);	
  	TPad *pad20 = new TPad(name,name,0.053+2*width,vertical,0.053+3*width,0.99,0,0,0);pad20->SetTopMargin(0.05);pad20->SetBottomMargin(0.0);pad20->SetLeftMargin(0.0);pad20->SetRightMargin(0.0);pad20->Draw();
	pad20->cd();
	//   Fourth panel
  	top->cd();
	sprintf(name,"pad3%d",number);	
  	TPad *pad30 = new TPad(name,name,0.053+3*width,vertical,0.053+4*width,0.99,0,0,0);pad30->SetTopMargin(0.05);pad30->SetBottomMargin(0.0);pad30->SetLeftMargin(0.0);pad30->SetRightMargin(0.0);pad30->Draw();
	pad30->cd();
	
	//   This is just for the Y axis label
  	top->cd();
	sprintf(name,"padh%d",number);	
  	TPad *padh3 = new TPad(name,name,0.,0.01,0.25,vertical,0,0,0);padh3->SetTopMargin(0.00);padh3->SetBottomMargin(0.13);padh3->SetLeftMargin(0.214);padh3->SetRightMargin(0.0);padh3->Draw();
	padh3->cd();
	//   First panel
  	top->cd();
	sprintf(name,"pad4%d",number);	
  	TPad *pad40 = new TPad(name,name,0.053,0.01,0.053+width,vertical,0,0,0);pad40->SetTopMargin(0.00);pad40->SetBottomMargin(0.13);pad40->SetLeftMargin(0.0);pad40->SetRightMargin(0.0);pad40->Draw();
	pad40->cd();
	//   Second panel
  	top->cd();
	sprintf(name,"pad5%d",number);	
  	TPad *pad50 = new TPad(name,name,0.053+width,0.01,0.053+2*width,vertical,0,0,0);pad50->SetTopMargin(0.00);pad50->SetBottomMargin(0.13);pad50->SetLeftMargin(0.0);pad50->SetRightMargin(0.0);pad50->Draw();
	pad50->cd();
	//   Third panel
  	top->cd();
	sprintf(name,"pad6%d",number);	
  	TPad *pad60 = new TPad(name,name,0.053+2*width,0.01,0.053+3*width,vertical,0,0,0);pad60->SetTopMargin(0.00);pad60->SetBottomMargin(0.13);pad60->SetLeftMargin(0.0);pad60->SetRightMargin(0.0);pad60->Draw();
	pad60->cd();
	//   Fourth panel
  	top->cd();
	sprintf(name,"pad7%d",number);	
  	TPad *pad70 = new TPad(name,name,0.053+3*width,0.01,0.053+4*width,vertical,0,0,0);pad70->SetTopMargin(0.00);pad70->SetBottomMargin(0.13);pad70->SetLeftMargin(0.0);pad70->SetRightMargin(0.0);pad70->Draw();
	pad70->cd();
}

void mcInv(int number = 0, int check = 0, TH2D* h = NULL){
	char name[100];
	sprintf(name,"cc%d",number);
	TCanvas *c = new TCanvas(name,name,0,0,2800,1200);
  	gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
  	gStyle->SetMarkerSize(1.6);

  	c->cd();
  	TPad *top = new TPad("top","top",0.01,0.01,0.99,0.99,0,0,0);
  	top->SetTopMargin(0.0);top->SetBottomMargin(0.0);top->SetLeftMargin(0.0);top->SetRightMargin(0.0);top->Draw();
	top->cd();
	
	//Left pad
	sprintf(name,"padleft%d",number);	
  	TPad *padleft = new TPad(name,name,0.01,0.01,0.49,0.99,0,0,0);padleft->SetTopMargin(0.05);padleft->SetBottomMargin(0.13);padleft->SetLeftMargin(0.18);padleft->SetRightMargin(0.0);padleft->Draw();
	padleft->cd();
	top->cd();

	//Right pad
	sprintf(name,"padright%d",number);	
  	TPad *padright = new TPad(name,name,0.5,0.01,0.99,0.99,0,0,0);padright->SetTopMargin(0.05);padright->SetBottomMargin(0.13);padright->SetLeftMargin(0.14);padright->SetRightMargin(0.05);padright->Draw();
	padright->cd();
	h->Draw();

		padright->cd();
		sprintf(name,"padtl%d",number);	
		TPad *padtl = new TPad(name,name,0.01,0.54,0.55,0.95,0,0,0);padtl->SetTopMargin(0.0);padtl->SetBottomMargin(0.0);padtl->SetLeftMargin(0.2);padtl->SetRightMargin(0.0);padtl->Draw();
		padtl->cd();
		padright->cd();
		sprintf(name,"padtr%d",number);	
		TPad *padtr = new TPad(name,name,0.55,0.54,0.99,0.95,0,0,0);padtr->SetTopMargin(0.0);padtr->SetBottomMargin(0.0);padtr->SetLeftMargin(0.0);padtr->SetRightMargin(0.0);padtr->Draw();
		padtr->cd();

		padright->cd();
		sprintf(name,"padbl%d",number);	
		TPad *padbl = new TPad(name,name,0.01,0.13,0.55,0.54,0,0,0);padbl->SetTopMargin(0.0);padbl->SetBottomMargin(0.0);padbl->SetLeftMargin(0.2);padbl->SetRightMargin(0.0);padbl->Draw();
		padbl->cd();
		padright->cd();
		sprintf(name,"padbr%d",number);	
		TPad *padbr = new TPad(name,name,0.55,0.13,0.99,0.54,0,0,0);padbr->SetTopMargin(0.0);padbr->SetBottomMargin(0.0);padbr->SetLeftMargin(0.0);padbr->SetRightMargin(0.0);padbr->Draw();
		padbr->cd();
	
}
void mc2x2(int number = 0, int check = 0){
	//============================================================================
	// 							 Four Panel
	//============================================================================
	char name[100];
	sprintf(name,"cc%d",number);
	TCanvas *c = new TCanvas(name,name,0,0,800,800);
  	gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
  	gStyle->SetMarkerSize(1.6);
  	c->cd();
  	TPad *top = new TPad("top","top",0.01,0.01,0.99,0.99,0,0,0);
  	top->SetTopMargin(0.0);top->SetBottomMargin(0.0);top->SetLeftMargin(0.0);top->SetRightMargin(0.0);top->Draw();
	top->cd();
	//   This is just for the Y axis label
  	top->cd();
	double width = 0.45;
	sprintf(name,"padl%d",number);	
  	TPad *padl3 = new TPad(name,name,0.01,0.5,0.25,0.99,0,0,0);padl3->SetTopMargin(0.05);padl3->SetBottomMargin(0.00);padl3->SetLeftMargin(0.295);padl3->SetRightMargin(0.0);padl3->Draw();
	padl3->cd();
	top->cd();
	sprintf(name,"padh%d",number);	
  	TPad *padh3 = new TPad(name,name,0.01,0.01,0.25,0.5,0,0,0);padh3->SetTopMargin(0.00);padh3->SetBottomMargin(0.15);padh3->SetLeftMargin(0.295);padh3->SetRightMargin(0.0);padh3->Draw();
	padh3->cd();
	//   First panel
  	top->cd();
	sprintf(name,"pad0%d",number);	
  	TPad *pad00 = new TPad(name,name,0.08,0.5,0.08+width,0.99,0,0,0);pad00->SetTopMargin(0.05);pad00->SetBottomMargin(0.00);pad00->SetLeftMargin(0.0);pad00->SetRightMargin(0.0);pad00->Draw();
	pad00->cd();
	//   Second panel
  	top->cd();
	sprintf(name,"pad1%d",number);	
  	TPad *pad10 = new TPad(name,name,0.08+width,0.5,0.08+2*width,0.99,0,0,0);pad10->SetTopMargin(0.05);pad10->SetBottomMargin(0.0);pad10->SetLeftMargin(0.0);pad10->SetRightMargin(0.0);pad10->Draw();
	pad10->cd();
	//   Third panel
  	top->cd();
	sprintf(name,"pad2%d",number);	
  	TPad *pad20 = new TPad(name,name,0.08,0.01,0.08+width,0.5,0,0,0);pad20->SetTopMargin(0.00);pad20->SetBottomMargin(0.15);pad20->SetLeftMargin(0.0);pad20->SetRightMargin(0.0);pad20->Draw();
	pad20->cd();
	//   Fourth panel
  	top->cd();
	sprintf(name,"pad3%d",number);	
  	TPad *pad30 = new TPad(name,name,0.08+width,0.01,0.08+2*width,0.5,0,0,0);pad30->SetTopMargin(0.00);pad30->SetBottomMargin(0.15);pad30->SetLeftMargin(0.0);pad30->SetRightMargin(0.0);pad30->Draw();
	pad30->cd();
	//White pad:
	top->cd();
  	TPad *padw = new TPad("padw","padw",0.06,0.48,0.079,0.52,0,0,0);padw->SetTopMargin(0.00);padw->SetBottomMargin(0.00);padw->SetLeftMargin(0.0);padw->SetRightMargin(0.0);
	padw->SetBorderMode(1);
	padw->SetBorderSize(1);
	if(check){
		padw->Draw(); padw->cd();	
	} 
}
void mc4(int number = 0, double width = 0.235, int x = 1300, int y = 400){
	//============================================================================
	// 							 Four Panel
	//============================================================================
	char name[100];
	sprintf(name,"cc%d",number);
	TCanvas *c = new TCanvas(name,name, 0, 0, x, y);
  	gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
  	gStyle->SetMarkerSize(1.6);
  	c->cd();
  	TPad *top = new TPad("top","top",0.01,0.01,0.99,0.99,0,0,0);
  	top->SetTopMargin(0.0);top->SetBottomMargin(0.0);top->SetLeftMargin(0.0);top->SetRightMargin(0.0);top->Draw();
	top->cd();
	//   This is just for the Y axis label
  	top->cd();
	sprintf(name,"padl%d",number);	
  	TPad *padl3 = new TPad(name,name,0.,0.01,0.25,0.99,0,0,0);padl3->SetTopMargin(0.05);padl3->SetBottomMargin(0.15);padl3->SetLeftMargin(0.212);padl3->SetRightMargin(0.0);padl3->Draw();
	padl3->cd();
	//   First panel
  	top->cd();
	sprintf(name,"pad0%d",number);	
  	TPad *pad00 = new TPad(name,name,0.053,0.01,0.053+width,0.99,0,0,0);pad00->SetTopMargin(0.05);pad00->SetBottomMargin(0.15);pad00->SetLeftMargin(0.0);pad00->SetRightMargin(0.0);pad00->Draw();
	pad00->cd();
	//   Second panel
  	top->cd();
	sprintf(name,"pad1%d",number);	
  	TPad *pad10 = new TPad(name,name,0.053+width,0.01,0.053+2*width,0.99,0,0,0);pad10->SetTopMargin(0.05);pad10->SetBottomMargin(0.15);pad10->SetLeftMargin(0.0);pad10->SetRightMargin(0.0);pad10->Draw();
	pad10->cd();
	//   Third panel
  	top->cd();
	sprintf(name,"pad2%d",number);	
  	TPad *pad20 = new TPad(name,name,0.053+2*width,0.01,0.053+3*width,0.99,0,0,0);pad20->SetTopMargin(0.05);pad20->SetBottomMargin(0.15);pad20->SetLeftMargin(0.0);pad20->SetRightMargin(0.0);pad20->Draw();
	pad20->cd();
	//   Fourth panel
  	top->cd();
	sprintf(name,"pad3%d",number);	
  	TPad *pad30 = new TPad(name,name,0.053+3*width,0.01,0.053+4*width,0.99,0,0,0);pad30->SetTopMargin(0.05);pad30->SetBottomMargin(0.15);pad30->SetLeftMargin(0.0);pad30->SetRightMargin(0.0);pad30->Draw();
	pad30->cd();
}
void mcNxM(int number = 0, int N = 9, int M = 8, int xp = 0, int yp = 0, int x = 400, int y = 400){
    char name[100];
    sprintf(name,"cc%d",number);
	TPad *Pads[N][M];
	TCanvas* cc = new TCanvas(name,name, xp, yp, x, y);
  	gStyle->SetOptStat(0); gStyle->SetOptTitle(0);
  	gStyle->SetMarkerSize(1.6);
	cc->cd();
  	TPad *top = new TPad("top","top",0.02,0.02,0.99,0.99,0,0,0);
  	top->Range(0, 0, 1, 1);
  	top->Draw();
  	top->cd();
	double margin = 0.25;
	double width = 0.98/((double)(N)+margin);
	double heigh = 0.98/((double)(M)+margin);
	for(int iy = 0; iy < M; iy++){
		for(int ix = 0; ix < N; ix++){
			top->cd();
			double x = width;
			double y = heigh;
			double extrax = 0;
			double extray = 0;
			if(ix == 0) extrax += margin*width;
			if(iy == M-1) extray += margin*heigh;
			Pads[ix][iy] = new TPad( Form("Pads%dX%dY%d", number, ix, iy) , Form("Pads%dX%dY%d", number, ix, iy), 0.995-(N-ix)*x-extrax, 0.995-(iy+1)*y-extray, 0.995-(N-1-ix)*x, 0.995-iy*y, 0, 0, 0);
			Pads[ix][iy]->SetLeftMargin(0.001);
			Pads[ix][iy]->SetRightMargin(0.001);
			Pads[ix][iy]->SetTopMargin(0.001);
			Pads[ix][iy]->SetBottomMargin(0.001);
			Pads[ix][iy]->SetLogy(1);
			if(ix == 0) Pads[ix][iy]->SetLeftMargin(margin);
			if(iy == M-1) Pads[ix][iy]->SetBottomMargin(margin);
			Pads[ix][iy]->Draw();
			Pads[ix][iy]->cd();	
		}
	}
    gSystem->ProcessEvents();
}
void mpadlog(int number, int x, int y, int z){
	if(x == 0 || x == 1) gROOT->ProcessLine( Form("mpad%d->SetLogx(%d)", number, x) );			
	if(y == 0 || y == 1) gROOT->ProcessLine( Form("mpad%d->SetLogy(%d)", number, y) );			
	if(z == 0 || z == 1) gROOT->ProcessLine( Form("mpad%d->SetLogz(%d)", number, z) );			
}
void mpadgrid(int number, int x, int y){
	if(x == 0 || x == 1) gROOT->ProcessLine( Form("mpad%d->SetGridx(%d)", number, x) );			
	if(y == 0 || y == 1) gROOT->ProcessLine( Form("mpad%d->SetGridy(%d)", number, y) );			
}
void mupdate(int number){
	gROOT->ProcessLine( Form("cc%d->Update()", number) );			
	gSystem->ProcessEvents();
}
/*void hset(TH1 *href, TString x, TString y, double xtitoff = 1.0,  double ytitoff = 1., double xtitsize = 0.06,  double ytitsize = 0.07){
  href->GetXaxis()->CenterTitle(1);
  href->GetYaxis()->CenterTitle(1);
  href->GetXaxis()->SetTitleOffset(xtitoff);
  href->GetYaxis()->SetTitleOffset(ytitoff);
  href->GetXaxis()->SetTitleSize(xtitsize);
  href->GetYaxis()->SetTitleSize(ytitsize);
  href->GetXaxis()->SetLabelOffset(0.01);
  href->GetYaxis()->SetLabelOffset(0.01);
  href->GetXaxis()->SetLabelSize(0.05);
  href->GetYaxis()->SetLabelSize(0.05);
  href->GetXaxis()->SetNdivisions(505);
  href->GetYaxis()->SetNdivisions(505);
  href->GetXaxis()->SetTitle(x);
  href->GetYaxis()->SetTitle(y);
}*/
void hset(TH1 *href, TString x, TString y, double xtitoff = 1.,  double ytitoff = 1., double xtitsize = 0.06,  double ytitsize = 0.07, double xlabsize = 0.05, double ylabsize = 0.05){
  href->GetXaxis()->CenterTitle(1);
  href->GetYaxis()->CenterTitle(1);
  //href->GetXaxis()->SetTitleColor(0);
  //href->GetYaxis()->SetTitleColor(0);
  //href->GetXaxis()->SetLabelColor(0);
  //href->GetYaxis()->SetLabelColor(0);
  href->GetXaxis()->SetTitleOffset(xtitoff);
  href->GetXaxis()->SetTitleOffset(xtitoff);
  href->GetYaxis()->SetTitleOffset(ytitoff);
  href->GetXaxis()->SetTitleSize(xtitsize);
  href->GetYaxis()->SetTitleSize(ytitsize);
  href->GetXaxis()->SetLabelOffset(0.01);
  href->GetYaxis()->SetLabelOffset(0.01);
  href->GetXaxis()->SetLabelSize(xlabsize);
  href->GetYaxis()->SetLabelSize(ylabsize);
  href->GetXaxis()->SetNdivisions(505);
  href->GetYaxis()->SetNdivisions(505);
  href->GetXaxis()->SetTitle(x);
  href->GetYaxis()->SetTitle(y);
}
void hset(TH2 *href, TString x, TString y, double xtitoff = 1.,  double ytitoff = 1., double xtitsize = 0.06,  double ytitsize = 0.07, double xlabsize = 0.05, double ylabsize = 0.05){
  href->GetXaxis()->CenterTitle(1);
  href->GetYaxis()->CenterTitle(1);
  //href->GetXaxis()->SetTitleColor(0);
  //href->GetYaxis()->SetTitleColor(0);
  //href->GetXaxis()->SetLabelColor(0);
  //href->GetYaxis()->SetLabelColor(0);
  href->GetXaxis()->SetTitleOffset(xtitoff);
  href->GetXaxis()->SetTitleOffset(xtitoff);
  href->GetYaxis()->SetTitleOffset(ytitoff);
  href->GetXaxis()->SetTitleSize(xtitsize);
  href->GetYaxis()->SetTitleSize(ytitsize);
  href->GetXaxis()->SetLabelOffset(0.01);
  href->GetYaxis()->SetLabelOffset(0.01);
  href->GetXaxis()->SetLabelSize(xlabsize);
  href->GetYaxis()->SetLabelSize(ylabsize);
  href->GetXaxis()->SetNdivisions(505);
  href->GetYaxis()->SetNdivisions(505);
  href->GetXaxis()->SetTitle(x);
  href->GetYaxis()->SetTitle(y);
}

void legi(int number = 0, const char * title = "", double xmin = 0.6, double ymin = 0.65, double xmax = 0.89, double ymax = 0.89){
	gROOT->ProcessLine(Form("TLegend *leg%d = new TLegend(%.3f,%.3f,%.3f,%.3f,\" %s \",\"brNDC\");", number, xmin, ymin, xmax, ymax, title) );
	gROOT->ProcessLine(Form("leg%d->SetFillStyle(0); leg%d->SetBorderSize(0); leg%d->SetTextSize(0.045);", number, number, number) );
	gROOT->ProcessLine(Form("leg%d->Draw()", number) );	
}
void legadd(int number, const char* item,  const char* desc, const char* option){	
	gROOT->ProcessLine(Form("leg%d->AddEntry(%s, \" %s \",\" %s \")", number, item, desc, option) );	
}
void style(TGraphErrors *tG, int marker, int color){
	tG->SetMarkerStyle(marker);
	tG->SetMarkerColor(color);
	tG->SetLineColor(color);
}
void BinLogX(TH1*h) 
{

   TAxis *axis = h->GetXaxis(); 
   int bins = axis->GetNbins();

   Axis_t from = axis->GetXmin();
   Axis_t to = axis->GetXmax();
   Axis_t width = (to - from) / bins;
   Axis_t *new_bins = new Axis_t[bins + 1];

   for (int i = 0; i <= bins; i++) {
     new_bins[i] = TMath::Power(10, from + i * width);
	 //cout << new_bins[i] << endl;
   } 
   axis->Set(bins, new_bins); 
   delete [] new_bins; 
}
void BinLogY(TH1*h) 
{

   TAxis *axis = h->GetYaxis(); 
   int bins = axis->GetNbins();

   Axis_t from = axis->GetXmin();
   Axis_t to = axis->GetXmax();
   Axis_t width = (to - from) / bins;
   Axis_t *new_bins = new Axis_t[bins + 1];

   for (int i = 0; i <= bins; i++) {
     new_bins[i] = TMath::Power(10, from + i * width);
   } 
   axis->Set(bins, new_bins); 
   delete [] new_bins; 
}
void DivideGraph(TGraphErrors* tG, TF1 *f){
	int N = tG->GetN();
	double *x = tG->GetX();
	double *y = tG->GetY();
	double *ey = tG->GetEY();
	for(int i = 0; i < N; i++){
		double ratio = f->Eval(x[i]);
		double error = ey[i]/y[i];
		y[i] = y[i]/ratio;
		ey[i] = error*y[i];
 	}
}
void DivideGraph(TGraph* tG, TF1 *f){
	int N = tG->GetN();
	double *x = tG->GetX();
	double *y = tG->GetY();
	for(int i = 0; i < N; i++){
		double ratio = f->Eval(x[i]);
		y[i] = y[i]/ratio;
 	}
}
void DivideGraph(TGraphErrors* tG1, TGraphErrors *tG2){
	int N = tG1->GetN();
	double *x = tG1->GetX();
	double *y1 = tG1->GetY();
	double *ey1 = tG1->GetEY();
	double *y2 = tG2->GetY();
	double *ey2 = tG2->GetEY();
	for(int i = 0; i < N; i++){
		y1[i] = y1[i]/y2[i];
 	}
}
void DivideGraph(TGraph* tG1, TGraph *tG2){
	int N = tG1->GetN();
	double *x = tG1->GetX();
	double *y1 = tG1->GetY();
	double *y2 = tG2->GetY();
	for(int i = 0; i < N; i++){
		y1[i] = y1[i]/y2[i];
 	}
}
void ScaleGraph(TGraphErrors* tG, double scale = 1.0){
	int N = tG->GetN();
	double *x = tG->GetX();
	double *y = tG->GetY();
	double *ey = tG->GetEY();
	for(int i = 0; i < N; i++){
		double error = ey[i]/y[i];
		y[i] = y[i]/scale;
		ey[i] = error*y[i];
 	}
}
