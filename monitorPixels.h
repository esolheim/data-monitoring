#ifndef monitorPixels_h
#define monitorPixels_h

#include <iostream>
#include <vector>
#include <algorithm>    
#include <map>

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TInterpreter.h"

#include "TStopwatch.h"



const int nCol = 3;
const int nRow = 6;

std::pair< int, int > chipMapL5[nRow][nCol] = // map of chips pair < feeid, chipid > for Layer 5
{

    std::make_pair(1,0), std::make_pair(1,1), std::make_pair(1,2),
    std::make_pair(0,0), std::make_pair(0,1), std::make_pair(0,2),
    std::make_pair(1,3), std::make_pair(1,4), std::make_pair(1,5), 
    std::make_pair(0,3), std::make_pair(0,4), std::make_pair(0,5),
    std::make_pair(1,6), std::make_pair(1,7), std::make_pair(1,8),
    std::make_pair(0,6), std::make_pair(0,7), std::make_pair(0,8)

};


std::pair< int, int > chipMapL10[nRow][nCol] = // map of chips pair < feeid, chipid > for Layer 10
{

    std::make_pair(3,6), std::make_pair(3,7), std::make_pair(3,8), 
    std::make_pair(2,6), std::make_pair(2,7), std::make_pair(2,8),
    std::make_pair(3,3), std::make_pair(3,4), std::make_pair(3,5),
    std::make_pair(2,3), std::make_pair(2,4), std::make_pair(2,5),
    std::make_pair(3,0), std::make_pair(3,1), std::make_pair(3,2),
    std::make_pair(2,0), std::make_pair(2,1), std::make_pair(2,2)

};


const int feeid_n = 4;
const int chipid_n = 9;



struct events
{

        ULong64_t    triggerid   ;

        std::vector< std::pair< uint16_t, uint16_t > > pixelhit       ;
        std::vector< std::pair< uint16_t, uint16_t > >* globalpixelhit ;

        int          chip        ; 
        unsigned int fee         ;
        uint8_t      headerFlag  ;
        uint8_t      trailerFlag ;    
        
};


bool compareEvents( events e1, events e2 )
{
    return ( e1.triggerid < e2.triggerid );
}
 


class monitorPixels
{

    private:

        std::vector< std::pair< uint16_t, uint16_t > >* pixelhits       = new std::vector< std::pair< uint16_t, uint16_t > >() ;
        std::vector< std::pair< uint16_t, uint16_t > >* globalpixelhits = new std::vector< std::pair< uint16_t, uint16_t > >() ;
    
        ULong64_t    trigger     ;
        uint8_t      chipid      ;
        unsigned int feeid       ;
        uint8_t      headerflag  ;
        uint8_t      trailerflag ;    


        std::vector< std::pair< uint16_t, uint16_t > > pixelhit_       ;

       // std::vector< std::pair< uint16_t, uint16_t > >& globalpixelhit_ ;

        std::vector<events> matchedEvents;


    public:

        TCanvas *cHitsL5 = new TCanvas( "cHitsL5", "cHitsL5", 1000, 1000 );
        TCanvas *cHitsChipsL5 = new TCanvas( "cHitsChipsL5", "cHitsChipsL5", 1000, 1000 );        
        TCanvas *cMapsChipsL5 = new TCanvas( "cMapsChipsL5", "cMapsChipsL5", 1000, 1000 );

        TCanvas *cHitsL10 = new TCanvas( "cHitsL10", "cHitsL10", 1000, 1000 );
        TCanvas *cHitsChipsL10 = new TCanvas( "cHitsChipsL10", "cHitsChipsL10", 1000, 1000 );        
        TCanvas *cMapsChipsL10 = new TCanvas( "cMapsChipsL10", "cMapsChipsL10", 1000, 1000 );


        TH1D* numberofhits_allChipsL5;
        TH1D* numberofhits_allChipsL10;
        TH1D* numberofhits[feeid_n][chipid_n];
        TH2D* pixelhitmap[feeid_n][chipid_n];


        std::vector<events> match( const char* hitFile );

        void fillHistograms( const std::vector<events> &matchedEvents );
        void drawHistograms();


        monitorPixels( std::vector< std::pair< uint16_t, uint16_t > >& pixelhit_ ) : pixelhit_( pixelhit_ ) {}


        monitorPixels( const char* hitFile);
        ~monitorPixels();

        

};


monitorPixels::monitorPixels( const char* hitFile )
{

    TStopwatch* timer = new TStopwatch();
    timer -> Start();

    
    match( hitFile );
    fillHistograms( matchedEvents );
    drawHistograms();


    timer->Stop();
    timer->Print();
    
}



monitorPixels::~monitorPixels()
{

    delete cHitsL5;
    delete cHitsChipsL5;
    delete cMapsChipsL5;

    delete cHitsL10;
    delete cHitsChipsL10;
    delete cMapsChipsL10;


    delete numberofhits_allChipsL5;
    delete numberofhits_allChipsL10;

    for ( int feeid = 0; feeid < feeid_n; feeid++ )
    {
        for ( int chipid = 0; chipid < chipid_n; chipid++ )
        {
            delete numberofhits[feeid][chipid];
            delete pixelhitmap[feeid][chipid];
            
        }
    }
    
}









#endif // monitorPixels_h